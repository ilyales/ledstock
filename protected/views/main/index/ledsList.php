<?php foreach ($products as $i=>$product): ?>
	<div class="maincat-item">
		<a href="<?php echo $product->photo2Url ?>" data-lightbox="name<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">
			<img class="maincat-item__img" src="<?php echo $product->photo2Url ?>">
		</a>
		<a class="maincat-item__name"  href="<?php echo $product->photo1Url ?>" data-lightbox="name<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">
			<?php echo $product->name ?>
		</a>
		<div class="maincat-item__values">
			<div class="maincat-item__values-line">
				<div class="maincat-item__values-element">Цвет</div>
				<div class="maincat-item__values-element2"><?php echo $product->color ?></div>
			</div>
			<div class="maincat-item__values-line">
				<div class="maincat-item__values-element">Led/м</div>
				<div class="maincat-item__values-element2"><?php echo $product->density ?></div>
			</div>
			<div class="maincat-item__values-line">
				<div class="maincat-item__values-element">Защита</div>
				<div class="maincat-item__values-element2"><?php echo $product->protection ?></div>
			</div>
			<div class="maincat-item__values-line">
				<div class="maincat-item__values-element">Ватт/м</div>
				<div class="maincat-item__values-element2"><?php echo $product->power ?></div>
			</div>
			<div class="maincat-item__values-line">
				<div class="maincat-item__values-element">Наличие</div>
				<div class="maincat-item__values-element2">
					<?php if ($product->stock==0):?>
							нет в наличии
					<?php else: ?>
						в наличии
					<?php endif; ?>					
				</div>
			</div>
		</div>
		<div class="maincat-item__price">
			<div class="maincat-item__price-element"><?php echo $product->price ?></div>
			<div class="maincat-item__price-button" onclick="window.orderPopup.show('<?php echo $product->name ?>','<?php echo $product->photo2ThmbUrl ?>')">Купить</div>
		</div>
	</div>
<?php endforeach; ?>