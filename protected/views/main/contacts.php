<div class="contacts">
	<div class="contacts__info">
		<div class="contacts__info-block">
			<div class="contacts__info-title contacts__info-title_address">г.Челябинск, ул. 40 лет Октября, д.33, офис 18</div>
			<div class="contacts__info-title contacts__info-title_phone">
				<a  class="contacts__info-phone-link" href="tel:89507493464">8-950-749-34-64</a>
			</div>
			<div class="contacts__info-title contacts__info-title_mail">
				<a href="mailto:infoxxx@led-stok.ru" class="contacts__info-mail-link" onmouseover="this.href=this.href.replace(/x/g,'');"></a>
			</div>
		</div>
		<div class="contacts__info-block">
			<div class="contacts__info-title contacts__info-title_time">Время работы офиса:</div>
			<div class="contacts__info-text contacts__info-text_time">Пн-пт - 10-18</div>
			<div class="contacts__info-text contacts__info-text_time">Сб, Вс - по договоренности</div>
		</div>
		<div class="contacts__info-block">
			<div class="contacts__info-title contacts__info-title_delivery">Доставляем светодиодные ленты по России.</div>
			<div class="contacts__info-text contacts__info-text_delivery">При заказе от 7000р – БЕСПЛАТНО. При меньшей сумме заказа доставка:</div>
			<div class="contacts__info-text contacts__info-text_delivery-item">- по Челябинску – 190р.</div>
			<div class="contacts__info-text contacts__info-text_delivery-item">- по Челябинской области – 200-400р.</div>
			<div class="contacts__info-text contacts__info-text_delivery-item">- по Уралу – 200-500р.</div>
			<div class="contacts__info-text contacts__info-text_delivery-item">- по России – 200-600р.</div>
		</div>
	</div>
	<div class="contacts__form">
		<div class="contacts__form-head">ОБРАТНАЯ СВЯЗЬ</div>
		<div class="contacts__form-inputs">
			<div class="contacts__form-inp formWrapName">
				<div class="contacts__form-inp-label">Имя <span class="contacts__form-inp-req formError">обязательное поле</span></div>
				<input type="text" class="contacts__form-inp-val formInput">
			</div>
			<div class="contacts__form-inp formWrapPhone">
				<div class="contacts__form-inp-label">Телефон <span class="contacts__form-inp-req formError">обязательное поле</span></div>
				<input type="text" class="contacts__form-inp-val formInput">
			</div>
			<div class="contacts__form-inp formWrapEmail">
				<div class="contacts__form-inp-label">Email <span class="contacts__form-inp-req formError">обязательное поле</span></div>
				<input type="text" class="contacts__form-inp-val formInput">
			</div>
			<div class="contacts__form-inp contacts__form-inp_action formWrapConfirm">
				<div class="contacts__form-confirmCehck-wrap ">
					<input type="checkbox" id="contacts__form-confirmCehck" class="contacts__form-confirmCehck formInput">
					<label for="contacts__form-confirmCehck" class="contacts__form-confirmCehck-label">Даю согласие на обработку <a href="/" class="contacts__form-confirmCehck-link">пересональных данных</a></label>
				</div>
				<div class="contacts__form-sendBtn formSendBtn">Отправить</div>
				<div class="contacts__form-inp-req contacts__form-inp-req_confirm">Необходимо дать согласие на обработку персональных данных</div>
			</div>
		</div>
		<div class="contacts__form-sending">
			<img src="/img/loading.gif" class="contacts__form-sending-img">
		</div>
		<div class="contacts__form-success">
			<div class="contacts__form-success-text">Сообщение отправлено!</div>
		</div>
		<div class="contacts__form-fail">
			<div class="contacts__form-fail-text">Произошла ошибка. Сообщение не доставлено.</div>
		</div>
	</div>

</div> 