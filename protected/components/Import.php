<?php
class Import 
{
	private $fileName;
	private $filePath;

	const ROW_DELIMITER = ";";
	const FIRST_ROW = 1;

	const LED_NAME_CLM = 0;
	const LED_DENSITY_CLM = 1;
	const LED_PROTECTION_CLM = 2;
	const LED_POWER_CLM = 3;
	const LED_COLOR_CLM = 4;
	const LED_STOCK_CLM = 5;
	const LED_PRICE_CLM = 6;
	const LED_PHOTO1_CLM = 7;
	const LED_PHOTO1_THMB_CLM = 8;
	const LED_PHOTO2_CLM = 9;
	const LED_PHOTO2_THMB_CLM = 10;
	const LED_PHOTO3_CLM = 11;


	const STOCK_YES = "в наличии";

	function __construct(){
		
	}

	private function createFilePath($fileName) {
		if ($fileName==null) {
			$this->fileName = "import.csv";
		}
		else {
			$this->fileName = $fileName;
		}
		$ds = DIRECTORY_SEPARATOR;
		$this->filePath = Yii::app()->basePath.$ds.'import'.$ds.$this->fileName;
	}

	public function ledsCsv($fileName=null,$fullPath=false) {
		if (!$fullPath) {
			$this->createFilePath($fileName);
		}
		else {
			$this->filePath = $fileName;
		}
		
		Leds::model()->deleteAll();
		$rows = file($this->filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		for ($i=self::FIRST_ROW;$i<=(count($rows)-self::FIRST_ROW);$i++) {
			$row = $rows[$i];
			$rowItems = explode(self::ROW_DELIMITER, $row);
			$ledItem = new Leds;
			$ledItem->name = $rowItems[self::LED_NAME_CLM];
			$ledItem->density = $rowItems[self::LED_DENSITY_CLM];
			$ledItem->protection = $rowItems[self::LED_PROTECTION_CLM];
			$ledItem->power = $rowItems[self::LED_POWER_CLM];
			$ledItem->color = $rowItems[self::LED_COLOR_CLM];
			$ledItem->price = $rowItems[self::LED_PRICE_CLM];


			if ($rowItems[self::LED_STOCK_CLM]==self::STOCK_YES) {
				$ledItem->stock = 1;
			}
			else {
				$ledItem->stock = 0;
			}
			
			$ledItem->photo1 = $rowItems[self::LED_PHOTO1_CLM];
			$ledItem->photo1_thmb = $rowItems[self::LED_PHOTO1_THMB_CLM];
			$ledItem->photo2 = $rowItems[self::LED_PHOTO2_CLM];
			$ledItem->photo2_thmb = $rowItems[self::LED_PHOTO2_THMB_CLM];
			$ledItem->photo3 = $rowItems[self::LED_PHOTO3_CLM];
			$ledItem->save();
			$errors = $ledItem->getErrors();
			if (count($errors)!=0) {
				$error = implode(';',array_keys($errors));
				throw new ImportException($i,$error);				
			}
		}
	}

	public function ledsExcel($fileName=null,$fullPath=false) {
		if (!$fullPath) {
			$this->createFilePath($fileName);
		}
		else {
			$this->filePath = $fileName;
		}
		Leds::model()->deleteAll();

		$objPHPExcel = PHPExcel_IOFactory::load($this->filePath);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$row_count = $objWorksheet->getHighestRow();

		for ($i=self::FIRST_ROW+1;$i<=$row_count;$i++)
		{

			$ledItem = new Leds;
			$ledItem->name = $objWorksheet->getCellByColumnAndRow(self::LED_NAME_CLM,$i)->getValue();
			$ledItem->density = $objWorksheet->getCellByColumnAndRow(self::LED_DENSITY_CLM,$i)->getValue();
			$ledItem->protection = $objWorksheet->getCellByColumnAndRow(self::LED_PROTECTION_CLM,$i)->getValue();
			$ledItem->power = $objWorksheet->getCellByColumnAndRow(self::LED_POWER_CLM,$i)->getValue();
			$ledItem->color = $objWorksheet->getCellByColumnAndRow(self::LED_COLOR_CLM,$i)->getValue();
			$ledItem->price = $objWorksheet->getCellByColumnAndRow(self::LED_PRICE_CLM,$i)->getValue();

			
			

			$stock = $objWorksheet->getCellByColumnAndRow(self::LED_STOCK_CLM,$i)->getValue();
			if ($stock==self::STOCK_YES) {
				$ledItem->stock = 1;
			}
			else {
				$ledItem->stock = 0;
			}
			
			$ledItem->photo1 = $objWorksheet->getCellByColumnAndRow(self::LED_PHOTO1_CLM,$i)->getValue();
			$ledItem->photo1_thmb = $objWorksheet->getCellByColumnAndRow(self::LED_PHOTO1_THMB_CLM,$i)->getValue();
			$ledItem->photo2 = $objWorksheet->getCellByColumnAndRow(self::LED_PHOTO2_CLM,$i)->getValue();
			$ledItem->photo2_thmb = $objWorksheet->getCellByColumnAndRow(self::LED_PHOTO2_THMB_CLM,$i)->getValue();
			$ledItem->photo3 = $objWorksheet->getCellByColumnAndRow(self::LED_PHOTO3_CLM,$i)->getValue();
			
			if ($ledItem->name!="" && $ledItem->name!=null) {
				$ledItem->save();
				$errors = $ledItem->getErrors();
				if (count($errors)!=0) {
					$error = implode(';',array_keys($errors));
					throw new ImportException($i,$error);				
				}
			}		
			
		}
	}	

}