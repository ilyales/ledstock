<?php
class CatalogManager 
{
	const LEDS_IMG_FOLDER_URL = "/catalog-imgs/leds/";

	public function getLeds($limit=null,$onlyInStock=false,$random=false){
		$criteria = new CDbCriteria;
		if ($limit!=null) {
			$criteria->limit = $limit;	
		}
		if ($onlyInStock) {
			$criteria->condition = "stock = 1";
		}
		if ($random) {
			$criteria->order = "RAND()";
		}
		$leds = Leds::model()->findAll($criteria);
		return $leds;
	}

}