<?php

class SystemController extends CController
{
	public function actionImportLeds(){
		$import = new Import;
		try {
			$import->ledsExcel('Sklad_Lenty.xlsx');
		}
		catch (ImportException $e) {
			echo "Error in line #".$e->errorRowNum." Message:".$e->getMessage();
		}
	}

	public function actionImport() {
		$this->layout = "/layouts/import";
		$phpexcel = new PHPExcel;
		
		if ($_FILES['userfile']['name']==null) {
			$this->render('import/importForm');
		}
		else {
			$uploadfile = Yii::app()->params['uploadsDir'].basename($_FILES['userfile']['name']);
			if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
				$import = new Import;
				try {
					$import->ledsExcel($uploadfile,true);
					$message = "Импорт завершён.\n";
				}
				catch (ImportException $e) {
					$message = "Error in line #".$e->errorRowNum." Message:".$e->getMessage();
				}
			    
			} else {
			    $message = "Ошибка загрузки файла\n";
			}			
			$this->render('import/importStatus',array('message'=>$message));
		}
		
	}

	
}