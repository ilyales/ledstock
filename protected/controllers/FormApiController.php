<?php

class FormApiController extends CController
{
	public function actionSend() {
		$mailer = new Mailer;
		$response = new AjaxResponse;
		try {
			$mailer->sendContacts($_POST);
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(),'error', 'mail');
			$response->setError($e->getMessage());
		}
		$response->send();
	}
}