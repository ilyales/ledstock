$(document).ready(function() {
	$('.slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade:true,
		arrows:false,
		dots:true,
		dotsClass:"slider__dots",
		autoplay:true,
		autoplaySpeed:6000,
	});
});
